/* Copyright © 2021 Taylor C. Richberger
 * This code is released under the license described in the LICENSE file
 */

#include "fuse_session.hxx"

namespace fuse {
    void SessionUnmounter::operator ()(fuse_session *session) {
        fuse_session_unmount(session);
    }

    void SessionDeleter::operator ()(fuse_session *session) {
        fuse_session_destroy(session);
    }

    Session::Session(fuse_args *args, const fuse_lowlevel_ops &ops, std::unique_ptr<Data> user_data) : session(fuse_session_new(args, &ops, sizeof(ops), user_data.get())), user_data(std::move(user_data)) {
        if (!session) {
            throw std::runtime_error("Could not initialize fuse session");
        }

        if (fuse_set_signal_handlers(session.get()) != 0) {
            throw std::runtime_error("Could not set fuse signal handlers");
        }
    }

    Session::~Session() {
        fuse_remove_signal_handlers(session.get());
    }

    Session::operator fuse_session *() {
        return session.get();
    }

    void Session::mount(const std::string &mountpoint) {
        if (fuse_session_mount(session.get(), mountpoint.c_str()) != 0) {
            throw std::runtime_error("Could not mount " + mountpoint);
        }
        mount_guard = MountGuard(session.get());
    }
}
