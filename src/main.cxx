/* Copyright © 2021 Taylor C. Richberger
 * This code is released under the license described in the LICENSE file
 */

#include "fuse.hxx"
#include "fuse_session.hxx"
#include "fuse_args.hxx"

#include <exception>
#include <stdexcept>
#include <iostream>
#include <string>
#include <string_view>
#include <memory>
#include <sys/stat.h>
#include <cstddef>
#include <vector>
#include <cstdlib>

namespace sf32f {
    struct opt_data {
        char *source_;

        opt_data() : source_(nullptr) {
        }

        ~opt_data() {
            if (source_ != nullptr) {
                std::free(source_);
            }
        }
    };

    static const struct fuse_opt opts[] = {
        { "source=%s", offsetof(opt_data, source_), 0 },
        FUSE_OPT_END
    };

    static void lookup(fuse_req_t req, fuse_ino_t parent, const char *name_raw)
    {
        const std::string_view name(name_raw);

        std::cout << "##### Doing lookup with parent " << parent << " and name " << name << std::endl;
        Data &data = *reinterpret_cast<Data *>(fuse_req_userdata(req));

        if (parent == 1) {
            data.seek_root_dir();
        } else {
            // "inodes" are stored as a parent directory cluster and entry count offset
            std::uint32_t parent_cluster_number = parent >> 32;
            std::uint32_t parent_entry_number = parent & 0xFFFFFFFF;
            data.seek_cluster(parent_cluster_number);
            data.skip(parent_entry_number * 32);
            char entry[32];
            data.read(entry, sizeof(entry));
            if (entry[0] == 0 || entry[0] == 0x2E || entry[0] == static_cast<char>(0xE5)) {
                std::cout << "FUSE ERROR 1" << std::endl;
                fuse_reply_err(req, ENOENT);
                return;
            }
            if ((entry[0x0B] & 0x10) == 0) {
                std::cout << "FUSE ERROR 2" << std::endl;
                fuse_reply_err(req, ENOTDIR);
                return;
            }

            const auto dir_cluster = static_cast<uint32_t>(entry[0x1A])
                | static_cast<uint32_t>(entry[0x1B]) << 8
                | static_cast<uint32_t>(entry[0x14]) << 16
                | static_cast<uint32_t>(entry[0x15]) << 24;

            data.seek_cluster(dir_cluster);
        }

        const auto dir_cluster_number = data.get_open_cluster();

        off_t entry_number = 0;
        while (true) {
            char entry[32];
            data.read(entry, sizeof(entry));
            if (entry[0] == 0) {
                std::cout << "FUSE ERROR 3" << std::endl;
                fuse_reply_err(req, ENOENT);
                return;
            }
            if (!(entry[0x0B] == 0x0F || entry[0] == 0x2E || entry[0] == static_cast<char>(0xE5))) {
                std::string filename(entry, 8);
                while (filename.size() > 0 && (filename.back() == '\0' || filename.back() == ' ')) {
                    filename.pop_back();
                }
                std::string_view short_extension(entry + 8, 3);
                while (short_extension.size() > 0 && (short_extension.back() == '\0' || short_extension.back() == ' ')) {
                    short_extension.remove_suffix(1);
                }
                if (!short_extension.empty()) {
                    filename.push_back('.');
                    filename += short_extension;
                }

                if (name == filename) {
                    struct fuse_entry_param e = {};
                    e.ino = static_cast<fuse_ino_t>(dir_cluster_number) << 32 | static_cast<fuse_ino_t>(entry_number);
                    e.attr.st_nlink = 1;
                    e.attr.st_ino = e.ino;
                    e.attr_timeout = 1.0;
                    e.entry_timeout = 1.0;

                    if ((entry[0x0B] & 0x01) == 0) {
                        e.attr.st_mode |= 0664;
                    } else {
                        e.attr.st_mode |= 0444;
                    }
                    if ((entry[0x0B] & 0x10) == 0) {
                        e.attr.st_mode |=  S_IFREG;
                    } else {
                        e.attr.st_mode |=  S_IFDIR | 0111;
                    }
                    e.attr.st_size = static_cast<uint32_t>(entry[0x1C])
                        | static_cast<uint32_t>(entry[0x1D]) << 8
                        | static_cast<uint32_t>(entry[0x1E]) << 16
                        | static_cast<uint32_t>(entry[0x1F]) << 24;
                    fuse_reply_entry(req, &e);
                    return;
                }
            }
            ++entry_number;
        }
    }

    static void getattr(fuse_req_t req, fuse_ino_t ino, struct fuse_file_info *fi)
    {
        std::cout << "##### Doing getattr with ino " << ino << std::endl;
        Data &data = *reinterpret_cast<Data *>(fuse_req_userdata(req));

        struct stat stbuf = {.st_ino = ino};
        if (ino == 1) {
            stbuf.st_mode = S_IFDIR | 0555;
            stbuf.st_nlink = 2;
        } else {
            stbuf.st_nlink = 1;

            // "inodes" are stored as a parent directory cluster and entry count offset
            std::uint32_t parent_cluster_number = ino >> 32;
            std::uint32_t parent_entry_number = ino & 0xFFFFFFFF;
            data.seek_cluster(parent_cluster_number);
            data.skip(parent_entry_number * 32);
            char entry[32];
            data.read(entry, sizeof(entry));
            if (entry[0] == 0 || entry[0] == 0x2E || entry[0] == static_cast<char>(0xE5)) {
                std::cout << "FUSE ERROR 4" << std::endl;
                fuse_reply_err(req, ENOENT);
                return;
            }

            const auto data_cluster = static_cast<uint32_t>(entry[0x1A])
                | static_cast<uint32_t>(entry[0x1B]) << 8
                | static_cast<uint32_t>(entry[0x14]) << 16
                | static_cast<uint32_t>(entry[0x15]) << 24;

            if ((entry[0x0B] & 0x01) == 0) {
                stbuf.st_mode |= 0664;
            } else {
                stbuf.st_mode |= 0444;
            }
            if ((entry[0x0B] & 0x10) == 0) {
                stbuf.st_mode |=  S_IFREG;
            } else {
                stbuf.st_mode |=  S_IFDIR | 0111;
            }
            stbuf.st_size = static_cast<uint32_t>(entry[0x1C])
                | static_cast<uint32_t>(entry[0x1D]) << 8
                | static_cast<uint32_t>(entry[0x1E]) << 16
                | static_cast<uint32_t>(entry[0x1F]) << 24;
        }

        fuse_reply_attr(req, &stbuf, 1.0);
    }

    static void readdir(fuse_req_t req, fuse_ino_t ino, size_t size, off_t off, struct fuse_file_info *fi)
    {
        std::cout << "##### Doing readdir with ino " << ino << std::endl;
        Data &data = *reinterpret_cast<Data *>(fuse_req_userdata(req));

        if (ino == 1) {
            data.seek_root_dir();
        } else {
            // "inodes" are stored as a parent directory cluster and entry count offset
            std::uint32_t parent_cluster_number = ino >> 32;
            std::uint32_t parent_entry_number = ino & 0xFFFFFFFF;
            data.seek_cluster(parent_cluster_number);
            data.skip(parent_entry_number * 32);
            char entry[32];
            data.read(entry, sizeof(entry));
            if (entry[0] == 0 || entry[0] == 0x2E || entry[0] == static_cast<char>(0xE5)) {
                std::cout << "FUSE ERROR 5" << std::endl;
                fuse_reply_err(req, ENOENT);
                return;
            }
            if ((entry[0x0B] & 0x10) == 0) {
                std::cout << "FUSE ERROR 6" << std::endl;
                fuse_reply_err(req, ENOTDIR);
                return;
            }

            const auto dir_cluster = static_cast<uint32_t>(entry[0x1A])
                | static_cast<uint32_t>(entry[0x1B]) << 8
                | static_cast<uint32_t>(entry[0x14]) << 16
                | static_cast<uint32_t>(entry[0x15]) << 24;

            data.seek_cluster(dir_cluster);
        }

        const auto dir_cluster_number = data.get_open_cluster();
        std::cout << "Seeked to cluster " << dir_cluster_number << std::endl;
        std::cout << "Skipping to offset " << off << std::endl;

        data.skip(off * 32);
        std::vector<char> buffer;

        off_t entry_number = off;

        while (true) {
            std::cout << "Reading entry " << entry_number << std::endl;
            char entry[32];
            data.read(entry, sizeof(entry));
            if (entry[0] == 0) {
                std::cout << "Entry was empty" << std::endl;
                break;
            }
            if (!(entry[0x0B] == 0x0F || entry[0] == 0x2E || entry[0] == static_cast<char>(0xE5))) {
                std::cout << "Entry was good" << std::endl;

                std::string filename(entry, 8);
                while (filename.size() > 0 && (filename.back() == '\0' || filename.back() == ' ')) {
                    filename.pop_back();
                }
                std::string_view short_extension(entry + 8, 3);
                while (short_extension.size() > 0 && (short_extension.back() == '\0' || short_extension.back() == ' ')) {
                    short_extension.remove_suffix(1);
                }
                if (!short_extension.empty()) {
                    filename.push_back('.');
                    filename += short_extension;
                }

                std::cout << "Found filename: " << filename << std::endl;

                const auto need_size = fuse_add_direntry(req, nullptr, 0, filename.c_str(), nullptr, 0);
                const auto buffer_prev_size = buffer.size();
                if (buffer_prev_size + need_size > size) {
                    break;
                }
                buffer.resize(buffer_prev_size + need_size, '\0');

                struct stat stbuf = {};
                stbuf.st_ino = static_cast<fuse_ino_t>(dir_cluster_number) << 32 | static_cast<fuse_ino_t>(entry_number);
                stbuf.st_nlink = 1;

                if ((entry[0x0B] & 0x01) == 0) {
                    stbuf.st_mode |= 0664;
                } else {
                    stbuf.st_mode |= 0444;
                }
                if ((entry[0x0B] & 0x10) == 0) {
                    stbuf.st_mode |=  S_IFREG;
                } else {
                    stbuf.st_mode |=  S_IFDIR;
                    stbuf.st_mode |=  0111;
                }
                stbuf.st_size = static_cast<uint32_t>(entry[0x1C])
                    | static_cast<uint32_t>(entry[0x1D]) << 8
                    | static_cast<uint32_t>(entry[0x1E]) << 16
                    | static_cast<uint32_t>(entry[0x1F]) << 24;

                fuse_add_direntry(req, buffer.data() + buffer_prev_size, buffer.size() - buffer_prev_size, filename.c_str(), &stbuf, entry_number + 1);
            }
            ++entry_number;
        }

        if (buffer.empty()) {
            fuse_reply_buf(req, buffer.data(), buffer.size());
        } else {
            fuse_reply_buf(req, buffer.data(), buffer.size());
        }
    }

    static const struct fuse_lowlevel_ops ops = {
        .lookup = lookup,
        .getattr = getattr,
        .readdir = readdir,
    };
}

int main(int argc, char **argv)
{
    try {
        fuse::Args args(argc, argv);
        auto fuse_opts = args.fuse_opts();

        if (fuse_opts.show_help) {
            std::cout << "usage: " << argv[0] << " [options] <mountpoint>\n\n" << std::flush;
            fuse_cmdline_help();
            fuse_lowlevel_help();
            return 0;
        } else if (fuse_opts.show_version) {
            std::cout << "FUSE library version " << fuse_pkgversion() << std::endl;
            fuse_lowlevel_version();
            return 0;
        }

        if (fuse_opts.mountpoint == nullptr) {
            throw std::runtime_error("Need to specify a mountpoint.  Try the --help option.");
        }

        sf32f::opt_data opt_data;

        if (fuse_opt_parse(args, reinterpret_cast<void *>(&opt_data), sf32f::opts, nullptr) == -1) {
            return 1;
        }

        if (opt_data.source_ == nullptr) {
            throw std::runtime_error("Need the source option.  Try the --help option.");
        } else {
            std::cout << "Source: " << opt_data.source_ << std::endl;
        }

        fuse::Session session(args, sf32f::ops, std::make_unique<Data>(opt_data.source_));

        session.mount(fuse_opts.mountpoint);
        fuse_daemonize(fuse_opts.foreground);

        if (fuse_opts.singlethread) {
            return fuse_session_loop(session);
        } else {
            fuse_loop_config config = {
                .clone_fd = fuse_opts.clone_fd,
                .max_idle_threads = fuse_opts.max_idle_threads,
            };

            return fuse_session_loop_mt(session, &config);
        }
    }
    catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }
}
