/* Copyright © 2021 Taylor C. Richberger
 * This code is released under the license described in the LICENSE file
 */

#include "data.hxx"

#include <iostream>
#include <stdexcept>
#include <algorithm>

Data::Data(const std::filesystem::path &path) : open_cluster(0) {
    volume.exceptions(std::fstream::badbit | std::fstream::failbit);
    volume.open(path, std::ios_base::in | std::ios_base::out | std::ios_base::binary);
    volume.seekg(0x0B, std::ios_base::beg);
    read_field(bytes_per_sector);
    read_field(sectors_per_cluster);
    read_field(reserved_sectors);
    read_field(file_allocation_tables);
    volume.seekg(0x20, std::ios_base::beg);
    read_field(sectors);
    read_field(sectors_per_fat);
    volume.seekg(0x2C, std::ios_base::beg);
    read_field(root_directory_cluster);
}

uint32_t Data::get_open_cluster() const noexcept {
    return open_cluster;
}

uint64_t Data::bytes_per_cluster() const noexcept {
    return bytes_per_sector * sectors_per_cluster;
}

uint64_t Data::bytes_per_fat() const noexcept {
    return sectors_per_fat * bytes_per_sector;
}

uint64_t Data::fat_bytes() const noexcept {
    return bytes_per_fat() * file_allocation_tables;
}

uint64_t Data::data_offset_bytes() const noexcept {
    return reserved_bytes() + fat_bytes();
}

uint64_t Data::reserved_bytes() const noexcept {
    return bytes_per_sector * reserved_sectors;
}

uint32_t Data::clusters() const noexcept {
    return sectors / sectors_per_cluster;
}

void Data::seek_root_dir() {
    seek_cluster(root_directory_cluster);
}

void Data::seek_cluster(const uint32_t cluster) {
    if (cluster < 2) {
        throw std::runtime_error("Illegal cluster number");
    }
    open_cluster = cluster;
    cluster_offset = 0;
    volume.seekg(data_offset_bytes() + bytes_per_cluster() * (cluster - 2), std::ios_base::beg);
}

void Data::read(char *buffer, size_t bytes) {
    size_t buffer_offset = 0;
    while (true) {
        const auto to_read_from_current = std::min<size_t>(bytes_per_cluster() - cluster_offset, bytes);
        volume.read(buffer + buffer_offset, to_read_from_current);
        bytes -= to_read_from_current;
        buffer_offset += to_read_from_current;
        cluster_offset += to_read_from_current;
        if (bytes > 0) {
            if (!seek_next_cluster()) {
                throw std::runtime_error("Could not read as much data as requested");
            }
        } else {
            break;
        }
    }
}

void Data::skip(size_t bytes) {
    std::cout << "Skipping " << bytes << " bytes" << std::endl;
    while (bytes > 0) {
        const auto left_in_current_cluster = bytes_per_cluster() - cluster_offset;
        std::cout << left_in_current_cluster << " left in current cluster" << std::endl;
        if (bytes > left_in_current_cluster) {
            bytes -= left_in_current_cluster;

            if (!seek_next_cluster()) {
                throw std::runtime_error("Could not read as much data as requested");
            }
        } else {
            volume.seekg(bytes, std::ios_base::cur);
            cluster_offset += bytes;
            return;
        }
    }
}

uint32_t Data::read_fat_entry(uint32_t cluster) {
    volume.seekg(reserved_bytes() + 4 * cluster, std::ios_base::beg);
    uint32_t data;
    read_field(data);
    return data;
}

bool Data::seek_next_cluster() {
    if (open_cluster < 2) {
        return false;
    }
    const auto next_cluster = read_fat_entry(open_cluster);
    if (next_cluster < 2) {
        throw std::runtime_error("Next cluster in chain was broken");
    }

    if (next_cluster >= 0x0FFFFF0) {
        return false;
    }

    seek_cluster(next_cluster);
    return true;
}
