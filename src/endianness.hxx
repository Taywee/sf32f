/* Copyright © 2021 Taylor C. Richberger
 * This code is released under the license described in the LICENSE file
 */
#pragma once

#include <cstddef>
#include <algorithm>

/** Swap bytes in place between CPU-native and little-endian.
 */
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#define le_swap(value)
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
template <typename T>
inline void le_swap(T &input) {
    if constexpr(sizeof(T) > 1) {
        using std::swap;
        char *input_buf = reinterpret_cast<char *>(&input);
        for (size_t i = 0; i < sizeof(T) / 2; ++i) {
            swap(input_buf[i], input_buf[sizeof(T) - i - 1]);
        }
    }
}
#else
#error NOT SUPPORTED ON PDP
#endif

template <typename T>
void to_le(const T input, char *output) {
    const char *input_buf = reinterpret_cast<const char *>(&input);
    for (size_t i = 0; i < sizeof(T); ++i) {
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
        output[i] = input_buf[i];
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
        output[sizeof(T) - i - 1] = input_buf[i];
#else
#error NOT SUPPORTED ON PDP
#endif
    }
}

template <typename T>
T from_le(const char *input) {
    T output;
    char *output_buf = reinterpret_cast<char *>(&output);
    for (size_t i = 0; i < sizeof(T); ++i) {
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
        output_buf[i] = input[i];
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
        output_buf[sizeof(T) - i - 1] = input[i];
#else
#error NOT SUPPORTED ON PDP
#endif
    }
    return output;
}
