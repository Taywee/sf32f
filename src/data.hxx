/* Copyright © 2021 Taylor C. Richberger
 * This code is released under the license described in the LICENSE file
 */
#pragma once

#include <byteswap.h>
#include <fstream>
#include <filesystem>
#include <mutex>
#include <cstdint>
#include <optional>

#include "endianness.hxx"

/** Private data class, for actually managing the filesystem.
 * Currently, everything is synchronous, and locked through a mutex.
 *
 * Inode numbers are indicated by parent directory cluster and entry offset.
 *
 * The root directory is treated specially.
 */
class Data {
    private:
        std::fstream volume;
        std::mutex mutex;

        uint16_t bytes_per_sector;
        uint8_t sectors_per_cluster;
        uint16_t reserved_sectors;
        uint8_t file_allocation_tables;
        uint32_t sectors;
        uint32_t sectors_per_fat;
        uint32_t root_directory_cluster;

        /** Bytes for all fats together.
         */
        uint64_t fat_bytes() const noexcept;
        uint64_t bytes_per_fat() const noexcept;
        uint64_t bytes_per_cluster() const noexcept;
        uint64_t reserved_bytes() const noexcept;
        uint32_t clusters() const noexcept;

        /** Offset of data section in bytes.
         */
        uint64_t data_offset_bytes() const noexcept;

        uint32_t open_cluster;
        uint32_t cluster_offset;

    public:
        Data(const std::filesystem::path &path);

        template <typename T>
        void read_field(T &field) {
            volume.read(reinterpret_cast<char *>(&field), sizeof(T));
            le_swap(field);
        }

        uint32_t get_open_cluster() const noexcept;

        void seek_root_dir();

        /** Seek to the numbered cluster.
         */
        void seek_cluster(uint32_t cluster);

        /** Try to read the requested number of bytes from the open cluster into
         * the buffer.
         *
         * Will automatically follow the FAT.
         */
        void read(char *buffer, size_t bytes);

        /** Try to skip the requested number of bytes from the open cluster into
         * the buffer.
         *
         * Will automatically follow the FAT.
         */
        void skip(size_t bytes);

        /** Read the entry from the FAT.
         */
        uint32_t read_fat_entry(uint32_t cluster);

        /** Seek to the next cluster in the FAT.
         *
         * Returns whether or not there was a next cluster.
         */
        bool seek_next_cluster();
};
