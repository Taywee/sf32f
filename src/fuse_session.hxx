/* Copyright © 2021 Taylor C. Richberger
 * This code is released under the license described in the LICENSE file
 */
#pragma once

#include "fuse.hxx"
#include "data.hxx"

#include <string>
#include <memory>
#include <stdexcept>

namespace fuse {
    struct SessionDeleter {
        void operator ()(fuse_session *session);
    };

    struct SessionUnmounter {
        void operator ()(fuse_session *session);
    };

    using MountGuard = std::unique_ptr<fuse_session, SessionUnmounter>;

    class Session {
        private:
            std::unique_ptr<fuse_session, SessionDeleter> session;
            std::unique_ptr<Data> user_data;
            MountGuard mount_guard;

        public:
            Session(fuse_args *args, const fuse_lowlevel_ops &ops, std::unique_ptr<Data> user_data);

            ~Session();

            operator fuse_session *();

            void mount(const std::string &mountpoint);
    };
    
}
