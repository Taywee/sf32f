/* Copyright © 2021 Taylor C. Richberger
 * This code is released under the license described in the LICENSE file
 */
#pragma once

#include "fuse.hxx"

namespace fuse {
    class Args {
        private:
            fuse_args args;

        public:
            Args(int argc, char **argv);
            ~Args();

            operator fuse_args *();

            fuse_cmdline_opts fuse_opts();
    };
}
