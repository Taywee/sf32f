/* Copyright © 2021 Taylor C. Richberger
 * This code is released under the license described in the LICENSE file
 */

#include "fuse_args.hxx"
#include <stdexcept>

namespace fuse {
    Args::Args(int argc, char **argv) : args(FUSE_ARGS_INIT(argc, argv)) {
        
    }

    Args::~Args() {
        fuse_opt_free_args(&args);
    }

    Args::operator fuse_args *() {
        return &args;
    }

    fuse_cmdline_opts Args::fuse_opts() {
        fuse_cmdline_opts opts = {};
        if (fuse_parse_cmdline(&args, &opts) != 0) {
            throw std::runtime_error("Could not parse args");
        }
        return opts;
    }
}
